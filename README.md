[![CC BY-ND 4.0][cc-by-nd-shield]][cc-by-nd]

This work is licensed under a [Creative Commons Attribution-NoDerivatives 4.0 International License][cc-by-nd].

[cc-by-nd]: http://creativecommons.org/licenses/by-nd/4.0/
[cc-by-nd-shield]: https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge

# LabGear Image Usage Documentation

## Introduction
This document provides detailed instructions on how to use the Docker image produced from the LabGear project's Dockerfile within GitLab CI/CD pipelines. The image is stored in the container registry at `registry.gitlab.com/japtain_cack/labgear`.

## Using LabGear Image in GitLab CI/CD

### Pulling the Image
To use the LabGear image in your GitLab CI/CD pipeline, you need to pull the image from the container registry. Here's an example of how to do this in a `.gitlab-ci.yml` job:

```yaml
image: registry.gitlab.com/japtain_cack/labgear:latest
```

### Tagging Strategy
LabGear uses semantic versioning for its Docker image tags. It's recommended to pin to a major or major.minor version to receive updates without breaking changes. Here's how the tagging is structured:

- `latest`: The most recent build.
- `1`: The latest `1.x.x` release.
- `1.2`: The latest `1.2.x` release.
- `1.2.3`: The specific `1.2.3` release.

### Using Tools in GitLab CI/CD
The following tools are used in the GitLab CI/CD pipeline:

- **hadolint**: A Dockerfile linter that helps you build best practice Docker images.
- **trivy**: A simple and comprehensive vulnerability scanner for containers.
- **kubeval**: Validates your Kubernetes configuration files against the Kubernetes schema.
- **kube-score**: Performs checks on your Kubernetes object definitions and gives each object a score.

Each tool adds value by ensuring the security and correctness of your Docker images and Kubernetes configurations.

### Example Job Using LabGear Image
Here's an example of a job in `.gitlab-ci.yml` that uses the LabGear image:

```yaml
release-version:
  stage: release
  image: registry.gitlab.com/japtain_cack/labgear:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+$/'
  needs:
    - build-versioned
  script:
    - |
      echo "running release_job for $CI_COMMIT_TAG"

      PREVIOUS_TAG=$(git describe --tags --abbrev=0 --always `git rev-list --tags --skip=1 --max-count=1` 2>/dev/null | cut -d \- -f 1)
      if [ -n "$PREVIOUS_TAG" ]; then
        echo "Changes since last release:"
        git log --pretty=format:"%h - %s (%an, %ar)" $PREVIOUS_TAG..$CI_COMMIT_TAG > changes.txt
      else
        echo "No tags found. Showing all commits."
        git log --pretty=format:"%h - %s (%an, %ar)" > changes.txt
      fi

      echo "CHANGES: $(cat changes.txt)"
  release:
    tag_name: '$CI_COMMIT_REF_NAME'
    description: |
      # release for $CI_COMMIT_REF_NAME.

      ## Changes
      <pre>
      $(cat changes.txt)
      </pre>

      ## Docker Image
      <pre>
      docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
      </pre>
    ref: '$CI_COMMIT_SHA'
```

## Conclusion
The LabGear image is designed to be used within GitLab CI/CD pipelines, providing a consistent environment for your builds, tests, and releases. By following the tagging strategy and utilizing the provided tools, you can maintain a high-quality codebase with automated checks and deployments.

Remember to replace `latest` with the appropriate version tag to pin to a specific version of the LabGear image. This ensures that you benefit from updates while avoiding unexpected changes from newer versions.

---

**Note**: This documentation focuses on the usage of the LabGear image and its application within GitLab CI/CD pipelines. It assumes familiarity with Docker and GitLab CI/CD concepts and does not cover the Dockerfile build process in detail. For more information on building Docker images, please refer to the official Docker documentation.
