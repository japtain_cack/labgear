# Stage 1: Base image
FROM registry.gitlab.com/gitlab-org/release-cli:latest AS base
RUN apk add --no-cache curl git bash

# Stage 2: Final image
FROM registry.gitlab.com/gitlab-org/release-cli:latest

# Copy necessary files from base image
COPY --from=base /usr/bin/curl /usr/bin/curl
COPY --from=base /usr/bin/git /usr/bin/git
COPY --from=base /usr/lib /usr/lib
COPY --from=base /bin/bash /bin/bash

CMD ["/bin/bash"]
